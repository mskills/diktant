package game

import (
	"diktant/internal/storage"
	"fmt"
)

func MoveToText(m *storage.Move) string {
	var direction string
	switch m.Direction {
	case storage.Up:
		direction = "вверх"
	case storage.Down:
		direction = "вниз"
	case storage.Left:
		direction = "влево"
	case storage.Right:
		direction = "вправо"
	}
	return fmt.Sprint(direction, ", ", m.Step)
}
