package game

import (
	"context"
	"diktant/internal/storage"
	"fmt"

	"github.com/DesSolo/marusia"
)

// хендлер по умолчанию
func Default(req *marusia.Request, resp *marusia.Response, _ context.Context) {
	var message string
	if req.IsNewSession() {
		message = `Чтобы начать игру скажи "новая игра" когда закончишь шаг - скажи "дальше"`
	} else {
		message = "Кажется я не расслышала, повтори пожалуйста снова"
	}
	resp.Text(message)
	resp.TTS(message)
}

// новая игра
func NewGame(gs storage.GameStorage, ss storage.SessionStorage) marusia.HandlerFunc {
	return func(req *marusia.Request, resp *marusia.Response, _ context.Context) {
		game, err := gs.GetRandomGame()
		if err != nil {
			message := "Не удалось загрузить новую игру"
			resp.Text(message)
			resp.TTS(message)
			return
		}

		session := storage.NewSession(req.Session.UserID, game)

		if err := ss.Add(session); err != nil {
			message := "Не удалось создать новую сессию"
			resp.Text(message)
			resp.TTS(message)
			return
		}

		message := fmt.Sprintf("Отступи слева %d и сверху %d, поставь точку и начни рисовать", game.InitialPosition.Left, game.InitialPosition.Up)
		resp.Text(message)
		resp.TTS(message)
	}
}

// следующий шаг
func NextStep(req *marusia.Request, resp *marusia.Response, ctx context.Context) {
	session := ctx.Value(SessionCTX).(*storage.Session)
	mv := session.NextMove()
	if mv == nil {
		End(req, resp, ctx)
		return
	}

	message := MoveToText(mv)
	resp.Text(message)
	resp.TTS(message)
}

// подсказка
func Hint(req *marusia.Request, resp *marusia.Response, ctx context.Context) {
	session := ctx.Value(SessionCTX).(*storage.Session)
	message := fmt.Sprintf(`Это "%s". Сейчас ты на шаге %d из %d`, session.Game.Name, session.Steps.Current, session.Steps.Total-1)
	resp.Text(message)
	resp.TTS(message)
}

// завершение игры
func End(req *marusia.Request, resp *marusia.Response, ctx context.Context) {
	session := ctx.Value(SessionCTX).(*storage.Session)
	message := fmt.Sprintf(`Отлично! Это "%s". Если хочешь сыграть еще раз - скажи "новая игра"`, session.Game.Name)
	resp.Text(message)
	resp.TTS(message)
	resp.EndSession()
}
