package game

import (
	"context"
	"diktant/internal/storage"
	"strings"

	"github.com/DesSolo/marusia"
)

type ContextKey string

const (
	SessionCTX ContextKey = "session"
)

func CurrentSessionCTX(ss storage.SessionStorage, defaultHandler marusia.HandlerFunc, ignore ...string) func(next marusia.HandlerFunc) marusia.HandlerFunc {
	return func(next marusia.HandlerFunc) marusia.HandlerFunc {
		return func(req *marusia.Request, resp *marusia.Response, ctx context.Context) {
			text := strings.ToLower(req.OriginalUtterance())

			for _, i := range ignore {
				if i == text {
					next(req, resp, ctx)
					return
				}
			}

			session, err := ss.Get(req.Session.UserID)
			if err != nil {
				message := "Не удалось определить сессию"
				resp.Text(message)
				resp.TTS(message)
				return
			}

			if session == nil {
				defaultHandler(req, resp, ctx)
				return
			}

			c := context.WithValue(ctx, SessionCTX, session)
			next(req, resp, c)
		}
	}
}
