package memory

import (
	"diktant/internal/storage"
	"encoding/json"
	"io/ioutil"
	"math/rand"
	"time"
)

type MemoryGamesStorage struct {
	games []*storage.Game
}

func NewMemoryGamesStorage() *MemoryGamesStorage {
	rand.Seed(time.Now().UnixNano())
	return &MemoryGamesStorage{}
}

// LoadJSON load all available games from json file
func (m *MemoryGamesStorage) LoadJSON(filePath string) error {
	f, err := ioutil.ReadFile(filePath)
	if err != nil {
		return err
	}

	if err := json.Unmarshal(f, &m.games); err != nil {
		return err
	}

	return nil

}

func (m *MemoryGamesStorage) GetRandomGame() (*storage.Game, error) {
	i := rand.Intn(len(m.games))
	return m.games[i], nil
}

type MemorySessionStorage struct {
	store map[string]*storage.Session
}

func NewMemorySessionStorage() *MemorySessionStorage {
	return &MemorySessionStorage{
		store: make(map[string]*storage.Session),
	}
}

func (m *MemorySessionStorage) Add(s *storage.Session) error {
	m.store[s.UserID] = s

	return nil
}

func (m *MemorySessionStorage) Get(id string) (*storage.Session, error) {
	return m.store[id], nil
}
