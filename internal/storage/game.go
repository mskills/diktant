package storage

// Step ...
type Step string

const (
	// Up ...
	Up Step = "T"
	// Down ...
	Down Step = "D"
	// Right ...
	Right Step = "R"
	// Left ...
	Left Step = "L"
)

type Move struct {
	Direction Step `json:"direction"`
	Step      int  `json:"step"`
}

type Game struct {
	Name            string `json:"name"`
	Category        string `json:"category"`
	InitialPosition struct {
		Left int `json:"left"`
		Up   int `json:"up"`
	} `json:"initial_position"`
	Moves []Move `json:"moves"`
}

type GameStorage interface {
	GetRandomGame() (*Game, error)
}

type Session struct {
	UserID string
	Game   *Game
	Steps  struct {
		Total   int
		Current int
		IsEnd   bool
	}
}

func (s *Session) CurrentMove() *Move {
	return &s.Game.Moves[s.Steps.Current]
}

func (s *Session) NextMove() *Move {
	nextStep := s.Steps.Current + 1
	if nextStep < s.Steps.Total {
		s.Steps.Current = nextStep
		return s.CurrentMove()
	}

	s.Steps.IsEnd = true
	return nil
}

func NewSession(userID string, g *Game) *Session {
	s := Session{
		UserID: userID,
		Game:   g,
	}
	s.Steps.Total = len(g.Moves)
	s.Steps.Current = 0
	s.Steps.IsEnd = false

	return &s
}

type SessionStorage interface {
	Add(*Session) error
	Get(id string) (*Session, error)
}
