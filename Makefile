PROJECT_NAME=$(shell basename "$(PWD)")
MAIN_FILE=cmd/main.go
BIN_DIR=bin

clean:
	rm -rf ${BIN_DIR}

build: clean
	CGO_ENABLED=0 go build -ldflags "-s -w" -o ${BIN_DIR}/${PROJECT_NAME} ${MAIN_FILE}

run:
	go run ${MAIN_FILE}