### Diktant
Навык для маруси "графический диктант"

## Env
|Key|Default|Description|
|---|---|---|
|GAMES_STORAGE_TYPE| memory | Тип хранилища для игр |
|GAMES_STORAGE_FILE_PATH | _no_ | Путь к файлу с играми |
|SESSION_STORAGE_TYPE| memory | Тип хранилища пользовательских сессий|
|SKILL_USE_SSL| false | Использовать SSL |
|SKILL_SSL_CERT_FILE | _no_ | Путь к сертификату |
|SKILL_SSL_KEY_FILE | _no_ | Путь к ключу |
|SKILL_ADDR| :9000 | Прослушиваемый порт |
|SKILL_WEBHOOK_URI| /webhook | location |