module diktant

go 1.15

require (
	github.com/DesSolo/marusia v0.0.0-20210818202437-faff4d67a529
	github.com/stretchr/testify v1.7.0
)
