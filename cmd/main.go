package main

import (
	"diktant/internal/env"
	"diktant/internal/game"
	"diktant/internal/storage"
	"diktant/internal/storage/memory"
	"fmt"
	"log"
	"strconv"

	"github.com/DesSolo/marusia"
	"github.com/DesSolo/marusia/middlewares"
)

func loadGamesStorage() (storage.GameStorage, error) {
	st := env.GetEnvOrDefault("GAMES_STORAGE_TYPE", "memory")

	switch st {
	case "memory":
		st := memory.NewMemoryGamesStorage()
		fp := env.GetEnvOrFault("GAMES_STORAGE_FILE_PATH")
		if err := st.LoadJSON(fp); err != nil {
			return nil, err
		}

		return st, nil
	}

	return nil, fmt.Errorf("games storage type: %s not supported", st)
}

func loadSessionsStorage() (storage.SessionStorage, error) {
	st := env.GetEnvOrDefault("SESSION_STORAGE_TYPE", "memory")

	switch st {
	case "memory":
		return memory.NewMemorySessionStorage(), nil
	}

	return nil, fmt.Errorf("sessions storage type: %s not supported", st)
}

func loadConfig() (*marusia.Config, error) {
	useSSL, err := strconv.ParseBool(env.GetEnvOrDefault("SKILL_USE_SSL", "false"))
	if err != nil {
		return nil, err
	}

	var certFile, keyFile, addr, webhookURL string

	if useSSL {
		certFile = env.GetEnvOrFault("SKILL_SSL_CERT_FILE")
		keyFile = env.GetEnvOrFault("SKILL_SSL_KEY_FILE")
	}

	addr = env.GetEnvOrDefault("SKILL_ADDR", ":9000")
	webhookURL = env.GetEnvOrDefault("SKILL_WEBHOOK_URI", "/webhook")

	return marusia.NewConfig(useSSL, certFile, keyFile, addr, webhookURL), nil
}

func main() {
	gs, err := loadGamesStorage()
	if err != nil {
		log.Fatalf("fault load games storage err: %s", err)
	}

	ss, err := loadSessionsStorage()
	if err != nil {
		log.Fatalf("fault load sessions storage err: %s", err)
	}

	r := marusia.NewDialogRouter(true)
	r.RegisterDefault(game.Default)

	r.Use(middlewares.Logging)
	r.Use(game.CurrentSessionCTX(ss, game.Default, "новая игра"))

	r.Register("новая игра", game.NewGame(gs, ss))
	r.Register("дальше", game.NextStep)
	r.Register("что это", game.Hint)
	r.Register("пока", game.End)

	config, err := loadConfig()
	if err != nil {
		log.Fatal(err)
	}

	skill := marusia.NewSkill(config, r)
	if err := skill.ListenAndServe(); err != nil {
		log.Fatal(err)
	}
}
